# This file is part of the SV-Benchmarks collection of verification tasks:
# https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
#
# SPDX-FileCopyrightText: 2024 Marian Lingsch-Rosenfeld
#
# SPDX-License-Identifier: GPL-3.0-or-later

This directory contains witnesses and validation task
definition files for some of the programs in the parent
directory. The set is designed to verify that the validator
makes use of the information contained in the input witnesses 
and supports various features of the witness
format.