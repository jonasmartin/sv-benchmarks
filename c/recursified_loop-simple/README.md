<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://github.com/sosy-lab/sv-benchmarks

SPDX-FileCopyrightText: 2019-2022 Dirk Beyer, Matthias Dangl, Daniel Dietsch, Matthias Heizmann, Thomas Lemberger, and Michael Tautschnig

SPDX-License-Identifier: Apache-2.0
-->

Benchmark created the same way as in Dyck, F., Richter, C., Wehrheim, H. (2023). Robustness Testing of Software Verifiers. In: Ferreira, C., Willemse, T.A.C. (eds) Software Engineering and Formal Methods. SEFM 2023. Lecture Notes in Computer Science, vol 14323. Springer, Cham. https://doi.org/10.1007/978-3-031-47115-5_5

This benchmark was automatically generated by transforming tasks with loops from the loop-invariants category of the sv-competition into tasks with tail-recursive functions.


Original Benchmark readme:

Simple loop example programs.
 - deep-nested: Intended to show that execution-based witness validators are not always a good solution. Written by Philipp Berger, RWTH Aachen University.
 - nested: loops with constant bounds. Taken from CPAchecker.


For generating benchmark tasks from programs with loops, our tool employs the following
transformation:

Program with loop:
```C
...
while(condition(arg1, ..., argn))
    statement_or_block(arg1, ..., argn);
...
```

Transformed program with tail-recursive function:
```C
void recursive_loop(*p_arg1, ..., *p_argn) {
    if (condition(*p_arg1, ..., *p_argn)) {
        statement_or_block(*p_arg1, ..., *p_argn);
        recursive_loop(p_arg1, p_argn);
    }
}
...
recursive_loop(&arg1, ..., &argn);
...
```

For more information on the transformation process, please refer to our tool [semtransforms](https://github.com/FlorianDyck/semtransforms).


